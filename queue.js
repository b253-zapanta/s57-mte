let collection = [];

function print() {
  return collection;
}

function enqueue(x) {
  collection.push(x);
  console.log(collection);
  return collection;
}

function dequeue() {
  collection.shift();
  return collection;
}

function front() {
  return collection[0];
}

function size() {
  x = collection.length;
  return x;
}

function isEmpty() {
  return !collection;
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
